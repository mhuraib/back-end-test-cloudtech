var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var configDB = require('./config/database.js');
var apiController = require('./controllers/apiController');
var htmlController = require('./controllers/htmlController');

mongoose.Promise = global.Promise;
mongoose.connect(configDB.url);

require('./config/passport')(passport);

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.set('view engine','ejs');

// for passport
app.use(session({ secret: 'thisismysecret'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

apiController(app);
htmlController(app,passport);

app.listen(port);
console.log('Access localhost on port '+ port);