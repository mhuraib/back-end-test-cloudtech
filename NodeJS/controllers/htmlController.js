module.exports = function (app, passport){


app.route('/')
    .get(function(req, res) {
        res.render('index.ejs', {message: req.flash('notLoggedIn')});
    });


app.route('/login')
    .get(function(req, res) {
        res.render('login.ejs', {message: req.flash('loginMessage')});
    })
    .post(passport.authenticate('local-login', {
        successRedirect : '/api/view-list',
        failureRedirect : '/login',
        failureFlash : true
    }));


app.route('/signup')
    .get(function(req, res) {
        res.render('signup.ejs', {message: req.flash('signupMessage')});
    })
    .post(passport.authenticate('local-signup', {
        successRedirect : '/api/view-list',
        failureRedirect : '/signup',
        failureFlash : true
    }));

   

app.route('/logout')
    .get(function(req, res) {
        req.logout();
        res.redirect('/');
    });


function isLoggedIn(req, res){

    if(req.isAuthenticated())
        return true;
        
    req.flash('notLoggedIn','Access for registered members only!');    
    res.redirect('/');
}
    
}