var ToDoList = require('../models/toDoList');
var User = require('../models/user');

module.exports = function (app){
    // getting and viewing the todo list for the logged in user using his id from the session
    app.route('/api/view-list')

        .get(function(req,res){
            if(isLoggedIn(req,res)){
                ToDoList.find({userID : req.session.passport.user},function(err,toDoList){
                    if(err)
                        throw err;
                    res.render('my-list',{toDoList : toDoList});
                });
            }
        });
    // adding a new item to the todo list  
    app.route('/api/add-item')
    
        .get(function(req, res){
            res.render('add-item',{message : req.flash('InvalidItemMessage')});
         })

         .post(function(req,res){
            if(isLoggedIn(req,res)){
                // check if the newly submitted item is not empty
                if(req.body.item.length>0){
                    var newToDo = new ToDoList();
                    // retrieve the logged in user id form the session
                    newToDo.userID = req.session.passport.user;
                    newToDo.item = req.body.item;
                    newToDo.isDone = false;

                    newToDo.save(function(err){
                        if(err)
                            throw err;
                        res.redirect('/api/view-list');
                    });
                // if the newly submitted item is empty, return a warning message
                }else {
                    req.flash('InvalidItemMessage',"Item Can't Be Empty");
                    res.redirect('/api/add-item');
                }
            }      
    });
    // removing an item in the todo list by using its id     
    app.get('/api/remove/:id',function(req,res){
            
        ToDoList.findByIdAndRemove(req.params.id, function(err){
            if(err)
                throw err;
        })
        res.redirect("/api/view-list"); 
    });
    // updating isDone in a todo list item from true to false and vise versa, by providing the item's id and the new status of isDone
    app.get('/api/update/:id/:isDone', function(req, res){
        ToDoList.findByIdAndUpdate(req.params.id,{isDone : req.params.isDone},function(err){
            if(err)
                throw err;
        });
        res.redirect('/api/view-list');
    })
        

    // checking if the user is authenticated before providing a service, else return with a warning message  
    function isLoggedIn(req, res){

        if(req.isAuthenticated())
            return true;
        
        req.flash('notLoggedIn','Access for registered members only!');    
        res.redirect('/');
    }
}