var mongoose = require('mongoose');

listSchema = mongoose.Schema({
    userID : String,
    item : String,
    isDone : Boolean
});

module.exports = mongoose.model('ToDoList', listSchema);