﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDo.Models;
using Microsoft.AspNet.Identity;
using MySql.Data.MySqlClient;
using System.Data;

namespace ToDo.Controllers
{
    [Authorize]
    public class ToDoListController : Controller
    {
        // creating a connection to the database
        MySqlConnection conn = new MySqlConnection("datasource=todolist.czgqjw6a6wjp.us-west-2.rds.amazonaws.com;port=3306;username=huraibm;password=abcd1234%");
        
        public ToDoListController()
        {


        }

        // add a new todo list item
        public ActionResult AddItem()
        {
            
            if(Request.HttpMethod == "POST")
            {   
                //Checking if the user submitted an empty item
                if (Request["item"].Length > 0)
                {
                    // add the new todo item to the database
                    ToDoList.addItemToDB(conn, User.Identity.GetUserId(), Request["item"]);

                  
                    return RedirectToAction("ViewList", "ToDoList");
                }
                else
                {
                    // return with a warning message for submitting an empty item
                    ViewData["message"] = "Item Can't Be Empty";

                    
                    return View();
                }    
            }
            else
            {
                return View();
            }

            
        }
        // view the todo list for the logged in user
        public ActionResult ViewList()
        {
            //get the todo list for the logged in user and saving it in ViewData to use in the ViewList view
            ViewData["toDoList"] = ToDoList.getDataFromDB(conn, User.Identity.GetUserId());
            
            return View();
        }
        // updating isDone in a todo list item
        public ActionResult Update(int id, bool isDone)
        {

            ToDoList.updateItemInDB(conn, id, isDone);
            return RedirectToAction("ViewList", "ToDoList");
        }
        // removing and item from the todo list
        public ActionResult Remove(int id)
        {
            ToDoList.removeItemFromDB(conn, id);
            return RedirectToAction("ViewList", "ToDoList");

        }
    }
}