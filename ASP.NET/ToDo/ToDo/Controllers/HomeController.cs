﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
namespace ToDo.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            
           if(User.Identity.IsAuthenticated)
            {
                return RedirectToAction("ViewList", "ToDoList");
            }
            
            return View();
        }
    }
}