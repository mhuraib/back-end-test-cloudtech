﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace ToDo.Models
{
    public class ToDoList
    {   
        public int id { get; set; }

        public String UserID { get; set; }
    
        public String Item { get; set; }

        public Boolean IsDone { get; set; }

        // Adding a new todo item to the database
        public static void addItemToDB(MySqlConnection conn, String userID, String item)
        {
            string query = " USE todolist;INSERT INTO tododb (userId, item, isDone) VALUES ('" + userID + "', '" + item + "', 0)";
            excecuteQuery(conn, query);
        }

        // Getting the todo list for the logged in user
        public static ToDoList [] getDataFromDB(MySqlConnection conn, String userID)
        {
            DataTable dataTable = new DataTable();
            string query = "USE todolist;SELECT * FROM tododb WHERE userID = '" + userID + "'";
            MySqlCommand comm = new MySqlCommand(query, conn);
            conn.Open();
            // filling the result of the query from the database to dataTable variable
            using (MySqlDataAdapter data = new MySqlDataAdapter(comm))
            {
                data.Fill(dataTable);
            }

            ToDoList[] toDoList = new ToDoList[dataTable.Rows.Count];
            int counter = 0;

            // fiiling the toDoList array with the data stored in dataTable
            foreach (DataRow row in dataTable.Rows)
            {
                toDoList[counter] = new ToDoList();
                toDoList[counter].id = (int)row["id"];
                toDoList[counter].UserID = row["userID"].ToString();
                toDoList[counter].Item = row["item"].ToString();
                toDoList[counter].IsDone = (bool)row["isDone"];
                counter++;
            }
            conn.Close();

            return toDoList;
        }

        // updating the (isDone) attribute from true to false and vise versa in the database
        public static void updateItemInDB(MySqlConnection conn, int id, bool isDone)
        {
            string query = "USE todolist;UPDATE tododb SET isDone = " + isDone + " WHERE id = " + id;
            excecuteQuery(conn, query);
        }

        // removing a todo list item from the database using its id 
        public static void removeItemFromDB(MySqlConnection conn, int id)
        {
            string query = "USE todolist;DELETE FROM tododb WHERE id = " + id;
            excecuteQuery(conn, query);
        }
        // implementing all the procedures of excecuting a query
        private static void excecuteQuery(MySqlConnection conn, String query)
        {
            MySqlCommand comm = new MySqlCommand(query, conn);
            conn.Open();
            comm.ExecuteNonQuery();
            conn.Close();
        }
    }
}